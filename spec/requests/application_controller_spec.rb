require "rails_helper"

RSpec.describe "Application Controller", :type => :request do
  it "render" do
    get "/"
    expect(response).to have_http_status :ok
  end
end
