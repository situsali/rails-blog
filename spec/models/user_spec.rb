require 'rails_helper'

RSpec.describe User, type: :model do
  it 'has columns' do
    is_expected.to have_db_column(:id).of_type(:integer)
    is_expected.to have_db_column(:name).of_type(:string).with_options(null: false, limit: 64)
  end
end
